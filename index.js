'use strict'

var URL = require('url-parse')
var stackexchangeProvider = require('tiny-stackexchange-sdk')
var gitHubProvider = require('tiny-github-sdk')

var githubSource = 'github'
var stackexchangeSource = 'stackexchange'

function SourceData(source) {
    this.source = source
}

SourceData.prototype = {source: '', title: '', sourceId: 0, state: '', url: '', owner: '', repo: '', sourceSite: '', resolver: null}

function sourceDataProvider() {
}

function retrieveSourceProviderByUrl(url) {
    if (!url) {
        throw new Error('url is not defined')
    }
    var parsed = new URL(url)
    switch (parsed.hostname.toLowerCase()) {
        case 'github.com':
            return gitHubProvider
        default:
            return stackexchangeProvider
    }
}

function retrieveSourceProvider(sourceType) {
    switch (sourceType.toLowerCase()) {
        case githubSource:
            return gitHubProvider
        case stackexchangeSource:
            return stackexchangeProvider
        default:
            throw new Error('source type not supported')
    }
}

function rawToSourceData(rawData, sourceProviderType) {
    var source = new SourceData(sourceProviderType)
    if (sourceProviderType === githubSource) {
        source.title = rawData.title
        source.state = rawData.state
        source.sourceId = rawData.number
        source.url = rawData.html_url
        source.owner = rawData.owner
        source.repo = rawData.repo
    } else if (sourceProviderType === stackexchangeSource) {
        source.title = rawData.title
        source.sourceId = rawData.question_id
        source.state = (rawData.is_answered ? 'closed' : 'open')
        source.url = rawData.link
    }
    return source
}

function trySetResolver(sourceData) {
    if (sourceData.state === 'closed') {
        return tryGetResolver(sourceData)
            .then(function (resolveData) {
                if (resolveData && resolveData.resolved && resolveData.resolver) {
                    sourceData.resolver = resolveData.resolver
                }
            })
            .catch(function (err) {
                //log here
            })
            .then(function () {
                return sourceData
            })
    } else {
        return Promise.resolve(sourceData)
    }
}

function getSourceDataByUrl(url) {
    var parsedUrl = new URL(url)
    var sourceProvider = retrieveSourceProviderByUrl(url)
    var splittedPath = parsedUrl.pathname.split('/').filter(function (x) {
        return x
    })
    var request = null
    var providerType = null
    if (sourceProvider === gitHubProvider) {
        providerType = githubSource
        var owner = splittedPath[0],
            repo = splittedPath[1],
            issues = splittedPath[2],
            issueId = splittedPath[3]

        issueId = parseInt(issueId)
        console.log(owner, repo, issues, issueId)
        if (!(owner && repo && issues === 'issues' && !isNaN(issueId))) {
            return Promise.reject(new Error('Url is invalid'))
        }
        request = sourceProvider.getIssueInfo(owner, repo, issueId)
    } else if (sourceProvider === stackexchangeProvider) {
        providerType = stackexchangeSource
        if (splittedPath < 2 || splittedPath[0] !== 'questions') {
            return Promise.reject(new Error('Unable to get question by url'))
        }

        request = sourceProvider.getQuestionInfo(splittedPath[1], parsedUrl.hostname)
    } else {
        return Promise.reject(new Error('Source not supported'))
    }

    return request.then(function (issueData) {
        var sourceData = rawToSourceData(issueData, providerType)
        sourceData.sourceSite = parsedUrl.hostname.toLowerCase()
        return trySetResolver(sourceData)
    })
}

function getSourceData(inputSourceData) {
    var request = null
    if (inputSourceData.source === githubSource) {
        request = gitHubProvider.getIssueInfo(inputSourceData.owner, inputSourceData.repo, inputSourceData.sourceId)
    } else if (inputSourceData.source === stackexchangeSource) {
        request = stackexchangeProvider.getQuestionInfo(inputSourceData.sourceId, inputSourceData.sourceSite)
    }

    return request.then(function (issueData) {
        issueData.expirationDate = new Date(inputSourceData.expirationDate)
        var sourceData = rawToSourceData(issueData, inputSourceData.source)
        sourceData.sourceSite = inputSourceData.sourceSite
        return trySetResolver(sourceData)
    })
}

function tryGetResolver(inputSourceData) {
    var request = null
    if (inputSourceData.source === githubSource) {
        request = gitHubProvider.tryGetResolver(inputSourceData.owner, inputSourceData.repo, inputSourceData.sourceId)
    } else if (inputSourceData.source === stackexchangeSource) {
        request = stackexchangeProvider.tryGetResolver(inputSourceData.sourceId, inputSourceData.sourceSite)
    } else {
        throw new Error('source is not supported')
    }

    return request.then(function (resolveData) {
        return resolveData
    })
}

function getUsersByIds(userIds, provider, site) {
    try {
        var request = null
        if (provider === githubSource) {
            request = gitHubProvider.getUsersByIds(userIds)
        } else if (provider === stackexchangeSource) {
            request = stackexchangeProvider.getUsersByIds(userIds, site)
        } else {
            throw new Error('source is not supported')
        }

        return request
            .then(function (resolveData) {
                var mapFn = null
                if (provider === githubSource) {
                    mapFn = mapGitProfile
                } else if (provider === stackexchangeSource) {
                    mapFn = mapStackexchangeProfile
                }

                return resolveData.map(mapFn)
            })
    } catch (err) {
        return Promise.reject(err)
    }
}

function getUserById(userId, provider, site) {
    return getUsersByIds([userId], provider, site)
        .then(function (resolveData) {
            if (resolveData.length > 0) {
                return resolveData[0]
            } else {
                return null
            }
        })
}

function mapGitProfile(pd) {
    if (!pd) {
        return null
    }
    return {
        userId: pd.id,
        name: pd.name || pd.login,
        source: githubSource,
        avatar: pd.avatar_url,
        link: pd.html_url
    }
}

function mapStackexchangeProfile(pd) {
    if (!pd) {
        return null
    }
    return {
        userId: pd.user_id,
        name: pd.display_name,
        source: stackexchangeSource,
        avatar: pd.profile_image,
        link: pd.link
    }
}

function getUserProfile(provider, oAuthToken, sourceSite, key) {
    var request = null
    if (provider === githubSource) {
        request = gitHubProvider.getUserProfile(oAuthToken)
    } else if (provider === stackexchangeSource) {
        request = stackexchangeProvider.getUserProfile(oAuthToken, sourceSite, key)
    } else {
        throw new Error('source is not supported')
    }

    return request.then(function (profileData) {
        try {
            var error = profileData.error || profileData.error_message
            if (error) {
                throw new Error(error)
            }
            var mapFn = null
            if (provider === githubSource) {
                mapFn = mapGitProfile
            } else if (provider === stackexchangeSource) {
                mapFn = mapStackexchangeProfile
                if (profileData.items && profileData.items.length > 0) {
                    profileData = profileData.items[0]
                } else {
                    profileData = null
                }
            } else {
                throw new Error('source is not supported')
            }
            var profile = mapFn(profileData)
            if (sourceSite) {
                profile.site = sourceSite
            }
            return profile
        } catch (err) {
            return Promise.reject(err)
        }
    })
}

function getUserProfileByUrl(url) {
    try {
        if (!url) {
            throw new Error('url parameter is required')
        }
        var request = null
        var parsedUrl = new URL(url)
        var splittedPath = parsedUrl.pathname.split('/').filter(function (x) {
            return x
        })

        var mapFn = null
        var sourceProvider = retrieveSourceProviderByUrl(url)
        if (sourceProvider === gitHubProvider) {
            mapFn = mapGitProfile
            request = sourceProvider.getUserProfileByName(splittedPath[0])
        } else if (sourceProvider === stackexchangeProvider) {
            mapFn = mapStackexchangeProfile
            request = sourceProvider.getUsersByIds(splittedPath[1], parsedUrl.host)
        } else {
            throw new Error('Source is not supported')
        }

        return request.then(function (profileData) {
            var error = profileData.error || profileData.error_message
            if (error) {
                throw new Error(error)
            }
            if (profileData.length && profileData.length > 0) {
                profileData = profileData[0]
            }
            return mapFn(profileData)
        })
    } catch (err) {
        return Promise.reject(err)
    }
}

sourceDataProvider.prototype = {
    retrieveSourceProviderByUrl: retrieveSourceProviderByUrl,
    retrieveSourceProvider: retrieveSourceProvider,
    getSourceDataByUrl: getSourceDataByUrl,
    getSourceData: getSourceData,
    tryGetResolver: tryGetResolver,
    getUsersByIds: getUsersByIds,
    getUserById: getUserById,
    getUserProfile: getUserProfile,
    getUserProfileByUrl: getUserProfileByUrl
}

module.exports = new sourceDataProvider()